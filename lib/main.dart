import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

void main() => runApp(MyApp());

/*
 * Установка внутрь Scaffold
 */
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text('AppBar заголовок'),
          centerTitle: true,
        ),
        floatingActionButton: FloatingActionButton(
          child: Text('Plus'),
          onPressed: () {},
        ),
        body: SafeArea(
          left: true,
          top: true,
          right: true,
          bottom: true,
          //minimum: EdgeInsets.all(16.0), // отступ
          child: MyHomePage(),
        ),
      ),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(16.0),
      child: Column(
        children: [
          Text(
            'Тело Scaffold.',
            style: TextStyle(
              fontSize: 18.0,
              fontStyle: FontStyle.italic,
              fontWeight: FontWeight.w600,
              decoration: TextDecoration.underline,
              letterSpacing: 3.0,
              color: Colors.green,
              fontFamily: "BalsamiqSans",
            ),
          ),
          RichText(
            text: TextSpan(
              style: TextStyle(
                fontSize: 16.0,
                color: Colors.black87,
                fontFamily: "BalsamiqSans",
              ),
              children: <TextSpan>[
                TextSpan(text: "В этой строке "),
                TextSpan(
                  text: "важные ",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                TextSpan(text: "слова нужно выделить жирным шрифтом. "),
                TextSpan(text: "А вводные слова следует подчеркнуть, "),
                TextSpan(
                  text: "соответственно",
                  style: TextStyle(decoration: TextDecoration.underline),
                ),
                TextSpan(text: "."),
              ],
            ),
          ),
          // изображение из сети
          Image.network(
              "https://pbs.twimg.com/media/EBWSoDhXYAIXuBM.jpg:large"),
          //Image.asset("assets/images/hamster.jpg"),
          //SvgPicture.asset("assets/images/man.svg"),
        ],
      ),
    );
  }
}
